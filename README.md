#### Project Manager

###### Remote repository: 
https://gitlab.com/OlegGryazev/sp-07
###### Software requirements:
* JDK 8
* Apache Maven 3.6.3
* PostgreSQL 12
    
###### Technology stack:
* Maven
* Spring MVC
* Spring Security
* Spring Data JPA
* Spring Boot
* Hibernate
* Spring Testing
* REST-assured
    
###### Developer:
    Gryazev Oleg
    email: gryazev77@gmail.com
     
###### Build:
    mvn clean install
    
###### Run:
    java -jar target/projectmanager-1.7.jar