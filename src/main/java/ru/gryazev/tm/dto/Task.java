package ru.gryazev.tm.dto;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.service.IProjectService;
import ru.gryazev.tm.api.service.IUserService;
import ru.gryazev.tm.entity.TaskEntity;
import ru.gryazev.tm.enumerated.Status;

import java.util.Date;
import java.util.UUID;

@Getter
@Setter
public class Task {

    @NotNull
    private String id = UUID.randomUUID().toString();

    @Nullable
    private String projectId;

    @Nullable
    private String userId;

    @Nullable
    private String name;

    @Nullable
    private String details;

    @Nullable
    private Date dateStart;

    @Nullable
    private Date dateFinish;

    @NotNull
    private Status status = Status.PLANNED;

    @NotNull
    private Long createMillis = new Date().getTime();

    @Nullable
    public static TaskEntity toTaskEntity(
            final Task task,
            final IProjectService projectService,
            final IUserService userService
    ) {
        if (task == null) return null;
        @NotNull final TaskEntity taskEntity = new TaskEntity();
        taskEntity.setId(task.getId());
        taskEntity.setName(task.getName());
        taskEntity.setDetails(task.getDetails());
        if (task.getProjectId() != null) taskEntity
                .setProject(projectService.findById(task.getProjectId()));
        if (task.getUserId() != null) taskEntity
                .setUser(userService.findById(task.getUserId()));
        taskEntity.setDateFinish(task.getDateFinish());
        taskEntity.setDateStart(task.getDateStart());
        taskEntity.setStatus(task.getStatus());
        taskEntity.setCreateMillis(task.getCreateMillis());
        return taskEntity;
    }

}
