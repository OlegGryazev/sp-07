package ru.gryazev.tm.error;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class CrudNotFoundException extends RuntimeException {

    public CrudNotFoundException() {
        super("Entity not found.");
    }

}
