package ru.gryazev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.gryazev.tm.entity.ProjectEntity;

import java.util.List;
import java.util.Optional;

@Repository
public interface IProjectRepository extends CrudRepository<ProjectEntity, String> {
    
    @NotNull
    Optional<ProjectEntity> findById(@NotNull String id);

    @NotNull
    Optional<ProjectEntity> findByIdAndUserId(@NotNull String id, @NotNull String userId);

    @NotNull
    List<ProjectEntity> findByUserId(@NotNull String userId);

    void deleteById(@NotNull String id);

}
