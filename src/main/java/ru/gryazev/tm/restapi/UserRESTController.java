package ru.gryazev.tm.restapi;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import ru.gryazev.tm.api.service.IUserService;
import ru.gryazev.tm.dto.User;
import ru.gryazev.tm.entity.UserEntity;
import ru.gryazev.tm.enumerated.RoleType;
import ru.gryazev.tm.error.CrudNotFoundException;

import java.security.Principal;
import java.util.Map;

@RestController
public class UserRESTController {

    private final IUserService userService;

    @Autowired
    public UserRESTController(final IUserService userService) {
        this.userService = userService;
    }

    @RequestMapping(value = "rest/users/registration", method = RequestMethod.POST)
    public void createUser(@RequestBody final Map<String, String> credentials) {
        final String username = credentials.get("username");
        final String password = credentials.get("password");
        try {
            userService.findByUserName(username);
        } catch (CrudNotFoundException e) {
            userService.createUser(username, password, RoleType.USER);
        }
    }

    @RequestMapping(value = "rest/users/{username}", method = RequestMethod.GET)
    public User findUserByName(
            @PathVariable("username") final String username,
            final Principal principal
    ) {
        if (!principal.getName().equals(username)) return null;
        @Nullable final UserEntity userEntity = userService.findByUserName(username);
        return userEntity == null ? null : UserEntity.toUserDto(userEntity);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(value = "rest/users/delete/self", method = RequestMethod.DELETE)
    public void deleteSelf(final Principal principal) {
        @Nullable final UserEntity userEntity = userService.findByUserName(principal.getName());
        if (userEntity == null) return;
        userService.deleteById(userEntity.getId());
    }

}
