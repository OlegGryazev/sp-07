package ru.gryazev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.entity.UserEntity;
import ru.gryazev.tm.enumerated.RoleType;

public interface IUserService {

    @Nullable
    UserEntity createUser(@Nullable String username, @Nullable String password, @Nullable RoleType... roleTypes);

    @NotNull
    UserEntity findByUserName(@Nullable String username);

    @NotNull
    UserEntity findById(@Nullable String id);

    void deleteById(@Nullable String id);

}
