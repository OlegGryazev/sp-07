package ru.gryazev.tm.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import ru.gryazev.tm.entity.ProjectEntity;
import ru.gryazev.tm.entity.UserEntity;
import ru.gryazev.tm.repository.IProjectRepository;
import ru.gryazev.tm.repository.IUserRepository;

import java.util.Arrays;
import java.util.Optional;

@SpringBootTest
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
public class ProjectControllerUT {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private IUserRepository userRepository;

    @MockBean
    private IProjectRepository projectRepository;

    private UserEntity userEntity = new UserEntity();

    @Before
    public void setup() {
        userEntity.setUsername("test");
        final ProjectEntity projectEntity = new ProjectEntity();
        projectEntity.setName("testName");
        projectEntity.setDetails("testDetails");
        userEntity.setUsername("test");
        Mockito.when(userRepository.findByUsername("test")).thenReturn(userEntity);
        Mockito.when(projectRepository.findByIdAndUserId("testProjectId", userEntity.getId()))
                .thenReturn(Optional.of(projectEntity));
        Mockito.when(projectRepository.findByUserId(userEntity.getId()))
                .thenReturn(Arrays.asList(projectEntity));
    }

    @Test
    @WithMockUser("test")
    public void getProjects() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders.get("/projects"))
                .andExpect(MockMvcResultMatchers.view().name("project/projects"));
    }

    @Test
    @WithMockUser("test")
    public void getAddProject() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders.get("/add-project"))
                .andExpect(MockMvcResultMatchers.view().name("project/add-project"));
    }

    @Test
    @WithMockUser("test")
    public void getEditProjectPositive() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders
                        .get("/edit-project")
                        .param("projectId", "testProjectId")
                )
                .andExpect(MockMvcResultMatchers.view().name("project/edit-project"));
    }

    @Test
    @WithMockUser("test")
    public void getEditProjectNegative() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders
                        .get("/edit-project")
                        .param("projectId", "not_found")
                )
                .andExpect(MockMvcResultMatchers.status().is4xxClientError());
    }

    @Test
    @WithMockUser("test")
    public void getViewProjectPositive() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders
                        .get("/view-project")
                        .param("projectId", "testProjectId")
                )
                .andExpect(MockMvcResultMatchers.view().name("project/view-project"));
    }

    @Test
    @WithMockUser("test")
    public void getViewProjectNegative() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders
                        .get("/view-project")
                        .param("projectId", "not_found")
                )
                .andExpect(MockMvcResultMatchers.status().is4xxClientError());
    }

    @Test
    @WithMockUser("test")
    public void postDeleteProjectPositive() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders
                        .post("/project-remove")
                        .param("projectId", "testProjectId")
                )
                .andExpect(MockMvcResultMatchers.view().name("redirect:/projects"));
    }

    @Test
    @WithMockUser("test")
    public void postAddProjectPositive() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders
                        .post("/add-project")
                        .param("name", "test-name")
                        .param("details", "details")
                        .param("dateStart", "01-01-2020")
                        .param("dateFinish", "01-01-2020")
                        .param("status", "PLANNED")
                )
                .andExpect(MockMvcResultMatchers.view().name("redirect:/projects"));
    }

    @Test
    @WithMockUser("test")
    public void postEditProjectPositive() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders
                        .post("/edit-project")
                        .param("projectId", "testProjectId")
                        .param("name", "test-name")
                        .param("details", "details")
                        .param("dateStart", "01-01-2020")
                        .param("dateFinish", "01-01-2020")
                        .param("status", "PLANNED")
                )
                .andExpect(MockMvcResultMatchers.view().name("redirect:/projects"));
    }

}