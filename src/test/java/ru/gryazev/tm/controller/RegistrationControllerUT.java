package ru.gryazev.tm.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import ru.gryazev.tm.entity.UserEntity;
import ru.gryazev.tm.repository.IUserRepository;

@SpringBootTest
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
public class RegistrationControllerUT {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private IUserRepository userRepository;

    private UserEntity userEntity = new UserEntity();

    @Before
    public void setup() {
        userEntity.setUsername("test");
        Mockito.when(userRepository.findByUsername("test")).thenReturn(userEntity);
    }

    @Test
    public void getLogin() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders.get("/login"))
                .andExpect(MockMvcResultMatchers.view().name("login"));
    }

    @Test
    public void getRegistration() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders.get("/registration"))
                .andExpect(MockMvcResultMatchers.view().name("registration"));
    }

    @Test
    public void postRegistration() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders
                        .post("/registration")
                        .param("username", "newUser")
                        .param("password", "123")
                )
                .andExpect(MockMvcResultMatchers.view().name("redirect:/login"));
    }

    @Test
    public void postRegistrationNegative() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders
                        .post("/registration")
                        .param("username", "test")
                        .param("password", "123")
                )
                .andExpect(MockMvcResultMatchers.view().name("registration"));
    }

}