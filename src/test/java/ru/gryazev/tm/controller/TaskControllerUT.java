package ru.gryazev.tm.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import ru.gryazev.tm.entity.ProjectEntity;
import ru.gryazev.tm.entity.TaskEntity;
import ru.gryazev.tm.entity.UserEntity;
import ru.gryazev.tm.repository.IProjectRepository;
import ru.gryazev.tm.repository.ITaskRepository;
import ru.gryazev.tm.repository.IUserRepository;

import java.util.Arrays;
import java.util.Optional;

@SpringBootTest
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
public class TaskControllerUT {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private IUserRepository userRepository;

    @MockBean
    private IProjectRepository projectRepository;

    @MockBean
    private ITaskRepository taskRepository;

    private UserEntity userEntity = new UserEntity();

    @Before
    public void setup() {
        userEntity.setUsername("test");
        final ProjectEntity projectEntity = new ProjectEntity();
        projectEntity.setName("testName");
        projectEntity.setDetails("testDetails");
        userEntity.setUsername("test");
        final TaskEntity taskEntity = new TaskEntity();
        taskEntity.setName("testName");
        taskEntity.setDetails("testDetails");
        taskEntity.setProject(projectEntity);
        Mockito.when(userRepository.findByUsername("test")).thenReturn(userEntity);
        Mockito.when(taskRepository.findByIdAndUserId("testTaskId", userEntity.getId()))
                .thenReturn(Optional.of(taskEntity));
        Mockito.when(taskRepository.findByUserId(userEntity.getId()))
                .thenReturn(Arrays.asList(taskEntity));
        Mockito.when(projectRepository.findByUserId(userEntity.getId()))
                .thenReturn(Arrays.asList(projectEntity));
        Mockito.when(projectRepository.findById("linkedProjectId"))
                .thenReturn(Optional.of(projectEntity));
    }

    @Test
    @WithMockUser("test")
    public void getTasks() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders.get("/tasks"))
                .andExpect(MockMvcResultMatchers.view().name("task/tasks"));
    }

    @Test
    @WithMockUser("test")
    public void getAddTask() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders.get("/add-task"))
                .andExpect(MockMvcResultMatchers.view().name("task/add-task"));
    }

    @Test
    @WithMockUser("test")
    public void getEditTaskPositive() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders
                        .get("/edit-task")
                        .param("taskId", "testTaskId")
                )
                .andExpect(MockMvcResultMatchers.view().name("task/edit-task"));
    }

    @Test
    @WithMockUser("test")
    public void getEditTaskNegative() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders
                        .get("/edit-task")
                        .param("taskId", "not_found")
                )
                .andExpect(MockMvcResultMatchers.status().is4xxClientError());
    }

    @Test
    @WithMockUser("test")
    public void getViewTaskPositive() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders
                        .get("/view-task")
                        .param("taskId", "testTaskId")
                )
                .andExpect(MockMvcResultMatchers.view().name("task/view-task"));
    }

    @Test
    @WithMockUser("test")
    public void getViewTaskNegative() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders
                        .get("/view-task")
                        .param("taskId", "not_found")
                )
                .andExpect(MockMvcResultMatchers.status().is4xxClientError());
    }

    @Test
    @WithMockUser("test")
    public void postDeleteTaskPositive() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders
                        .post("/task-remove")
                        .param("taskId", "testTaskId")
                )
                .andExpect(MockMvcResultMatchers.view().name("redirect:/tasks"));
    }

    @Test
    @WithMockUser("test")
    public void postAddTaskPositive() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders
                        .post("/add-task")
                        .param("linkedProjectId", "linkedProjectId")
                        .param("name", "test-name")
                        .param("details", "details")
                        .param("dateStart", "01-01-2020")
                        .param("dateFinish", "01-01-2020")
                        .param("status", "PLANNED")
                )
                .andExpect(MockMvcResultMatchers.view().name("redirect:/tasks"));
    }

    @Test
    @WithMockUser("test")
    public void postEditProjectPositive() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders
                        .post("/edit-task")
                        .param("linkedProjectId", "linkedProjectId")
                        .param("taskId", "testTaskId")
                        .param("name", "test-name")
                        .param("details", "details")
                        .param("dateStart", "01-01-2020")
                        .param("dateFinish", "01-01-2020")
                        .param("status", "PLANNED")
                )
                .andExpect(MockMvcResultMatchers.view().name("redirect:/tasks"));
    }

}