package ru.gryazev.tm.security;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import ru.gryazev.tm.api.service.IProjectService;
import ru.gryazev.tm.api.service.ITaskService;
import ru.gryazev.tm.api.service.IUserService;
import ru.gryazev.tm.entity.ProjectEntity;
import ru.gryazev.tm.entity.TaskEntity;
import ru.gryazev.tm.entity.UserEntity;

import java.util.Arrays;

@SpringBootTest
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
public class SecurityUT {

    @Autowired
    private MockMvc mockMvc;

    private UserEntity userEntity = new UserEntity();

    @MockBean
    private IUserService userService;

    @MockBean
    private IProjectService projectService;

    @MockBean
    private ITaskService taskService;

    @Before
    public void setup() {
        final ProjectEntity projectEntity = new ProjectEntity();
        projectEntity.setName("testName");
        projectEntity.setDetails("testDetails");
        userEntity.setUsername("test");
        final TaskEntity taskEntity = new TaskEntity();
        taskEntity.setName("testName");
        taskEntity.setDetails("testDetails");
        taskEntity.setProject(projectEntity);
        Mockito.when(userService.findByUserName("test")).thenReturn(userEntity);
        Mockito.when(projectService.findByIdAndUserId("testProjectId", userEntity.getId()))
                .thenReturn(projectEntity);
        Mockito.when(projectService.findByUserId(userEntity.getId()))
                .thenReturn(Arrays.asList(projectEntity));
        Mockito.when(taskService.findByIdAndUserId("testTaskId", userEntity.getId()))
                .thenReturn(taskEntity);
        Mockito.when(taskService.findByUserId(userEntity.getId()))
                .thenReturn(Arrays.asList(taskEntity));
    }

    @Test
    public void getProjectsNegative() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders.get("/projects"))
                .andExpect(MockMvcResultMatchers.status().is4xxClientError());
    }

    @Test
    public void getAddProjectNegative() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders.get("/add-project"))
                .andExpect(MockMvcResultMatchers.status().is4xxClientError());
    }

    @Test
    public void getEditProjectNegative() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders.get("/edit-project"))
                .andExpect(MockMvcResultMatchers.status().is4xxClientError());
    }

    @Test
    public void PostDeleteProjectNegative() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders.post("/project-remove"))
                .andExpect(MockMvcResultMatchers.status().is4xxClientError());
    }

    @Test
    @WithMockUser("test")
    public void getProjectsPositive() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders.get("/projects"))
                .andExpect(MockMvcResultMatchers.view().name("project/projects"));
    }

    @Test
    @WithMockUser("test")
    public void getAddProjectPositive() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders.get("/add-project"))
                .andExpect(MockMvcResultMatchers.view().name("project/add-project"));
    }

    @Test
    @WithMockUser("test")
    public void getEditProjectPositive() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders
                        .get("/edit-project")
                        .param("projectId", "testProjectId")
                )
                .andExpect(MockMvcResultMatchers.view().name("project/edit-project"));
    }

    @Test
    @WithMockUser("test")
    public void PostDeleteProjectPositive() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders
                        .post("/project-remove")
                        .param("projectId", "testProjectId")
                )
                .andExpect(MockMvcResultMatchers.view().name("redirect:/projects"));
    }

    @Test
    public void getTasksNegative() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders.get("/tasks"))
                .andExpect(MockMvcResultMatchers.status().is4xxClientError());
    }

    @Test
    public void getAddTaskNegative() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders.get("/add-task"))
                .andExpect(MockMvcResultMatchers.status().is4xxClientError());
    }

    @Test
    public void getEditTaskNegative() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders.get("/edit-task"))
                .andExpect(MockMvcResultMatchers.status().is4xxClientError());
    }

    @Test
    public void PostDeleteTaskNegative() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders.post("/task-remove"))
                .andExpect(MockMvcResultMatchers.status().is4xxClientError());
    }

    @Test
    @WithMockUser("test")
    public void getTasksPositive() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders.get("/tasks"))
                .andExpect(MockMvcResultMatchers.view().name("task/tasks"));
    }

    @Test
    @WithMockUser("test")
    public void getAddTaskPositive() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders.get("/add-task"))
                .andExpect(MockMvcResultMatchers.view().name("task/add-task"));
    }

    @Test
    @WithMockUser("test")
    public void getEditTaskPositive() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders
                        .get("/edit-task")
                        .param("taskId", "testTaskId")
                )
                .andExpect(MockMvcResultMatchers.view().name("task/edit-task"));
    }

    @Test
    @WithMockUser("test")
    public void PostDeleteTaskPositive() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders
                        .post("/task-remove")
                        .param("taskId", "testTaskId")
                )
                .andExpect(MockMvcResultMatchers.view().name("redirect:/tasks"));
    }

}
