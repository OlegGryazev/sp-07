package ru.gryazev.tm.marshalling;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import ru.gryazev.tm.api.service.IProjectService;
import ru.gryazev.tm.api.service.IUserService;
import ru.gryazev.tm.entity.ProjectEntity;
import ru.gryazev.tm.entity.UserEntity;
import ru.gryazev.tm.enumerated.Status;

import java.util.ArrayList;

@SpringBootTest
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
public class MarshallingTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private IUserService userService;

    @MockBean
    private IProjectService projectService;

    private UserEntity userEntity = new UserEntity();

    private ProjectEntity projectEntity = new ProjectEntity();

    @Before
    public void setup() {
        userEntity.setUsername("test");
        initProjectEntity();
        Mockito.when(userService.findByUserName("test")).thenReturn(userEntity);
        Mockito.when(projectService.findByIdAndUserId(projectEntity.getId(), userEntity.getId()))
                .thenReturn(projectEntity);
    }

    public void initProjectEntity() {
        projectEntity.setUser(userEntity);
        projectEntity.setTasks(new ArrayList<>());
        projectEntity.setName("testProjectName");
        projectEntity.setDetails("testProjectDetails");
        projectEntity.setStatus(Status.PLANNED);
    }

    @Test
    @WithMockUser("test")
    public void getProjects() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders
                        .get("/rest/projects/" + projectEntity.getId())
                        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                )
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", Matchers.is(projectEntity.getId())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name", Matchers.is(projectEntity.getName())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.userId", Matchers.is(userEntity.getId())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.details", Matchers.is(projectEntity.getDetails())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.equalToIgnoringCase(projectEntity.getStatus().displayName())));

    }

}
