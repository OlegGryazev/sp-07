package ru.gryazev.tm.it;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.specification.RequestSpecification;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import ru.gryazev.tm.dto.Project;
import ru.gryazev.tm.dto.User;

import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class RestApiProjectsIT {

    @LocalServerPort
    int port;

    private Project project = new Project();

    private User user = new User();

    private RequestSpecification request;

    @Before
    public void init() {
        RestAssured.baseURI = "http://localhost/rest";
        RestAssured.port = port;
        user.setUsername("rest-assured-test-user");
        user.setPassword("1");
        userRegistration();
        final String userId = given()
                .auth().basic(user.getUsername(), user.getPassword())
                .get("/users/" + user.getUsername()).getBody().jsonPath().getString("id");
        user.setId(userId);
        project.setName("assured-test-project");
        project.setUserId(user.getId());
        request = RestAssured.given()
                .auth().basic(user.getUsername(), user.getPassword())
                .contentType(ContentType.JSON);
    }

    @After
    public void clear() {
        request.delete("/users/delete/self");
    }

    private void userRegistration() {
        final Map<String, String> requestBody = new HashMap<>();
        requestBody.put("username", user.getUsername());
        requestBody.put("password", user.getPassword());
        given()
                .contentType(ContentType.JSON)
                .body(requestBody)
                .when().post("/users/registration")
                .then().statusCode(HttpStatus.OK.value());
    }

    @Test
    @WithMockUser("rest-assured-test-user")
    public void testAddGetDeleteProjectOk() {
        request
                .body(project)
                .when().post("/projects/merge")
                .then().statusCode(HttpStatus.OK.value());

        request
                .when().get("/projects/" + project.getId())
                .then().body("name", equalTo("assured-test-project"));

        request
                .when().delete("/projects/delete/" + project.getId())
                .then().statusCode(HttpStatus.OK.value());
    }

    @Test
    @WithMockUser("rest-assured-test-user")
    public void testAddProject() {
        final String responseBefore = request.get("/projects").asString();
        final int projectsCountBefore = JsonPath.from(responseBefore).getList("").size();
        request
                .body(project)
                .when().post("/projects/merge")
                .then().statusCode(HttpStatus.OK.value());
        final String responseAfter = request.get("/projects").asString();
        final int projectsCountAfter = JsonPath.from(responseAfter).getList("").size();
        assertEquals(projectsCountAfter, projectsCountBefore + 1);
    }

    @Test
    @WithMockUser("rest-assured-test-user")
    public void testViewProjectNotFound() {
        request
                .when().get("/projects/test-assured-id")
                .then().statusCode(HttpStatus.NOT_FOUND.value());
    }

    @Test
    @WithMockUser("rest-assured-test-user")
    public void testMethodIsNotAllowed() {
        request
                .when().delete("/projects/" + "test-assured-id")
                .then().statusCode(HttpStatus.METHOD_NOT_ALLOWED.value());

        request
                .when().post("/projects")
                .then().statusCode(HttpStatus.METHOD_NOT_ALLOWED.value());

        request
                .when().post("/projects/delete/" + "test-assured-id")
                .then().statusCode(HttpStatus.METHOD_NOT_ALLOWED.value());

        request
                .when().get("/projects/delete/" + "test-assured-id")
                .then().statusCode(HttpStatus.METHOD_NOT_ALLOWED.value());

        request
                .when().delete("/projects/add" + "test-assured-id")
                .then().statusCode(HttpStatus.METHOD_NOT_ALLOWED.value());
    }

}
